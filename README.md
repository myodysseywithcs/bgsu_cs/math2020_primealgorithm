# MATH2020_Prim Algorithm
## Purpose
This program receive data from a csv file to generate a minimum spanning tree.  

## Procedures
Firstly this program read data from a csv file and build up a relation list, for example `(6,3)`.  
Then implement a matrix according to the list and do the following steps to generate a minimum spanning tree.  
- Step 1: Pick a new vertex that not in alpha but in the pool.  
- Step 2: Merge new vertex row and column with alpha row and column.  
- Step 3: put new relation in result list; Update alpha list and pool.  
The result should be in Result.csv
