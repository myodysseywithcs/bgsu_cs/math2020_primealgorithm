/*Name: Kefei Yang
Date:11/26/2017
Class:Math2220
purpose: this program receive from a .csv file to generate a minimal spinning tree.*/
#include "PrimAlgorithm.h"
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

int main()
{
	cout << "This program receive from a .csv file to generate a minimal spanning tree." << endl;

	PrimeAL mySolution;
	mySolution.solve();

	cout << "at the end of main" << endl;
	system("pause");
	return 0;
		
}
