#include "PrimAlgorithm.h"
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

void setUp::readFile(ifstream &infile)
{
	int maxRead = 0;
	int temp=0;
	const char *pTemp = nullptr;
	stringstream ss;
	string subStr1,subStr2;
	string str;
	
	//build up relation list
	getline(infile, str, ',');
	ss << str;
	ss >> root;
	infile.ignore();
	while(!infile.eof())
	{
		getline(infile, str, '\n');

		subStr1 = str.substr(0, str.find(","));
		if (subStr1 == "")
			break;
		pTemp = subStr1.c_str();
		temp = atoi(pTemp);
		myList.domain.push_back(temp);
		if (temp > maxRead)
			maxRead = temp;

		subStr2 = str.substr(str.find(",") + 1, str.find("\n"));
		if (subStr2 == "")
			break;
		pTemp = subStr2.c_str();
		temp = atoi(pTemp);
		myList.range.push_back(temp);
	}
	pTemp = nullptr;

	vertexNum = maxRead;

	//relation list to matrix
	//matrix resize
	myMatrix.row.resize(vertexNum,0);
	myMatrix.matrix.resize(vertexNum, myMatrix.row);
	vector<int>::iterator itR = myList.range.begin();
	vector<int>::iterator itD = myList.domain.begin();
	while (itD != myList.domain.end())
	{
		myMatrix.matrix[*itD-1][*itR-1] = 1;
		itD++;
		itR++;
	}
}

void setUp::printMatrix() {

	cout << "=====================================" << endl;
	cout << "Matrix" << endl;	
	for (int j = 0; j < myMatrix.matrix.size(); j++)
	{
		for (int i = 0; i < myMatrix.matrix.size(); i++)
			cout << myMatrix.matrix[i][j] << " ";
		cout << endl;
	}
	cout << "Matrix End" << endl;
	cout << "=====================================" << endl;
}

void setUp::printList() {
	vector<int>::iterator itD = myList.domain.begin();
	vector<int>::iterator itR = myList.range.begin();

	cout << "=====================================" << endl;
	cout << "Relation List" << endl;
	cout << "R = {";
	for (; itD != myList.domain.end() - 1; itD++, itR++)
		cout << "(" << *itD << "," << *itR << "),";
	cout << "(" << *itD << "," << *itR << ")";
	cout << "}" << endl;
	cout << "List End" << endl;
	cout << "=====================================" << endl;
}

void setUp::printOut() {
	ifstream infile;

	//read from file and setup
	infile.open("../include/Graph1.csv");
	if (!infile.is_open()) {
		cout << "Cannot open input file!!" << endl;
		system("pause");
		exit(0);
	}

	readFile(infile);
	infile.close();
	cout << "=====================================" << endl;
	cout << "This is a summary of the relation" << endl;
	cout << "The root is: " << printRoot() << endl;
	cout << "The vertex number is: " << printVertexNum() << endl;
	printList();
	printMatrix();
}


void PrimeAL::printVertexStatus() {
	cout << "This is Alpha: ";
	for (int i = 0; i < Alpha.size(); i++)
		cout << Alpha[i] << " ";
	cout << endl;

	cout << "This is the vertex Pool: ";
	for (int i = 0; i < vertexPool.size(); i++)
		cout << vertexPool[i] << " ";
	cout << endl;
}

void PrimeAL::initialize() {
	int b = 0;
	Alpha.push_back(mySetup.root);
	vertexPool.push_back(1);
	while (vertexPool.back() < mySetup.vertexNum)
		vertexPool.push_back(vertexPool.back() + 1);
	for (b = 0; vertexPool[b] != mySetup.root && b < mySetup.vertexNum; b++);
	vertexPool.erase(vertexPool.begin() + b);
}

int PrimeAL::pickBeta() {
	int b,c;

	//traverse alpha to pick one 
	for (int a = 0; a < Alpha.size(); a++)
	{
		//traverse the alpha[a] row
		for (b = 0; b < mySetup.myMatrix.matrix.size(); b++)
		{
			//check 1s in row alpha[a] 
			if (mySetup.myMatrix.matrix[Alpha[a] - 1][b] == 1)
			{
				b++;
				//check that b column is it exist in alpha already
				for (c = 0; c < Alpha.size() && b != Alpha[c]; c++);
				if (c == Alpha.size())
					return b;
				else
					continue;
			}
		}

	}

	cout << "No beta to pick now!" << endl;
}

void PrimeAL::printResult() {
	vector<int>::iterator itD = myResult.domain.begin();
	vector<int>::iterator itR = myResult.range.begin();

	cout << "Result of prime algorithm for minimal spanning tree." << endl;
	cout << "R = {";
	for (; itD != myResult.domain.end() - 1; itD++, itR++)
		cout << "(" << *itD << "," << *itR << "),";
	cout << "(" << *itD << "," << *itR << ")";
	cout << "}" << endl;
	cout << "List End" << endl;
	cout << "=====================================" << endl;
}

void PrimeAL::solve() {
	
	mySetup.printOut();
	system("pause");
	system("cls");

	initialize();
	while (vertexPool.size() != 0) 
	{
		int eraseCount;
		printVertexStatus();

		cout << "=====================================================================" << endl;
		cout << "Step 1: pick a new vertex that not in alpha but in the pool." << endl;
		beta = pickBeta();
		cout << "Step 2: Merge new vertex row and column with alpha row and column." << endl;
		cout << "Step 3: put new relation in result list; Update alpha list and pool" << endl;
		myResult.domain.push_back(Alpha.back());
		myResult.range.push_back(beta);
		Alpha.push_back(beta);
		for (eraseCount = 0; vertexPool[eraseCount] != beta && eraseCount < 100; eraseCount++);
		vertexPool.erase(vertexPool.begin() + eraseCount);
		cout << "=====================================================================" << endl;

		cout << "This is the result of this iteration: " << endl;
		cout << "New vertex: " << beta << endl;
		printResult();
		printVertexStatus();

		system("pause");
		system("cls");
	}
	cout << "\nAfter all iteration print out the result!" << endl;
	printResult();
	cout << "And then put result in 'Result.csv'.	" << endl;
	write2File();
}

void PrimeAL::write2File()
{
	ofstream outfile;
	outfile.open("../include/Result.csv");
	int i;

	if (!outfile.is_open()) {
		cout << "Cannot open output file!!" << endl;
		system("pause");
		exit(0);
	}
	outfile << "all relation in spanning tree is: " << endl;
	for (int i = 0; i < myResult.domain.size(); i++)
	{
		outfile << myResult.domain[i] << ","   ;
		outfile << myResult.range[i];
		outfile << endl;
	}
	outfile.close();


}
