#include <vector>
#include <fstream>

using namespace std;

struct RelationList {
	vector<int> domain;
	vector<int> range;
};
struct Matrix {
	vector<int> row;
	vector<vector<int>> matrix;
};

class setUp {
public:
	int root;
	int vertexNum;
	Matrix myMatrix;
	RelationList myList;

	int printVertexNum() { return vertexNum; }
	void readFile(ifstream&);
	void printMatrix();
	void printList();
	int printRoot() { return root; }
	void printOut();
};

class PrimeAL:setUp {
private:
	vector<int> rowTemp;
	vector<int> columnTemp;
	int beta;
	setUp mySetup;
	vector<int> Alpha;
	vector<int> vertexPool;
	RelationList myResult;
public:
	void initialize();
	void solve();
	int pickBeta();
	void printVertexStatus();
	void printResult();
	void write2File();
};